from datetime import datetime
import timeit


from sklearn import datasets


lfw_people = datasets.fetch_lfw_people(min_faces_per_person=60, resize=0.4)
#min_faces_per_person : the extracted dataset will only retain pictures of people that have at least min_faces_per_person different pictures.
#resize : Ratio used to resize the each face picture.
lfw_people_data = lfw_people.data[:, :2]
lfw_people_data = lfw_people_data.tolist()
lfw_people_len = len(lfw_people_data)
lfw_people_w = [100, 100]

wine = datasets.load_iris()
wine_data = wine.data[:, :2] #pourcentage d'alcool et d'acide malique
wine_data = wine_data.tolist()
wine_data_len = len(wine_data)
wine_w = [12.5, 1.2]

k_list = []
for k in range(1, len(wine_data), int(len(wine_data) / 20) + 1):
    k_list.append(k)
    print(k)
print(int(20.3))
print(int(20.7))
iris = datasets.load_iris()
iris_data = iris.data[:, :2]
iris_data = iris_data.tolist()
iris_data_len = len(iris_data)
iris_x = [2, 3]

k = 100

# kd tree
start_time = timeit.default_timer()

kd_tree = get_kd_tree(lfw_people_data)

ghfhg = 0

voisins1 = get_knn_kd(lfw_people_data, lfw_people_w, kd_tree, k)

time_x = (timeit.default_timer() - start_time)
print(": --- %s seconds ---" % (time_x))
kd_affiche(lfw_people_w, lfw_people_data, lfw_people_len, voisins1, k)

# knn naive
start_time2 = timeit.default_timer()

voisins2 = cherche_k_voisins(k, lfw_people_w, lfw_people_data, lfw_people_len)

time_y = (timeit.default_timer() - start_time2)
print(": --- %s seconds ---" % (time_y))
affiche(lfw_people_w, lfw_people_data, lfw_people_len, voisins2, k)

print(iris_data_len, wine_data_len, lfw_people_len)
